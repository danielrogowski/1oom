#ifndef INC_1OOM_GAME_NUMP_H
#define INC_1OOM_GAME_NUMP_H

extern void game_num_fixbugs(void);
extern void game_num_dump(void);
extern void game_num_extended(void);
extern void game_num_mod(void);

#endif
