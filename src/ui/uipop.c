#include "../game/game.h"
#include "../lib.h"
#include "../types.h"

char* ui_format_pop(int16_t pop)
{
    static char formatted_pop[sizeof(char) * 6];
    if (is_game_ruleset_extended)
    {
        lib_sprintf(formatted_pop, sizeof(formatted_pop), "%.1f\0", ((float) pop) / 10);
    }
    else
    {
        lib_sprintf(formatted_pop, sizeof(formatted_pop), "%i\0", pop);
    }
    return formatted_pop;
}
