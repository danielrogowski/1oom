#ifndef INC_1OOM_UIPOP_H
#define INC_1OOM_UIPOP_H

#include "../types.h"

// you don't own the pointer you get, don't free it!
extern const char* ui_format_pop(int16_t pop);

#endif